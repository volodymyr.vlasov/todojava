package com.example.service.impl;

import com.example.model.Task;
import com.example.repository.TaskRepository;
import com.example.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class MySqlTaskService implements TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    public MySqlTaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task getById(long id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public Task create(Task task) {
//
//        log.trace("");
//        log.debug("");
//        log.info("");
//        log.warn("");
//        log.error("");

        if (task.getId() == 0) {
            task.setCreatedDate(LocalDateTime.now());
            log.info("Created task {}", task);
            return taskRepository.save(task);
        }

        log.warn("Creation failed. Return null.");
        return null;
    }

    @Override
    public Task update(Task task) {
        if (taskRepository.existsById(task.getId())) {
            return taskRepository.save(task);
        }
        return null;
    }

    @Override
    public void deleteById(long id) {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
        }
    }
}
