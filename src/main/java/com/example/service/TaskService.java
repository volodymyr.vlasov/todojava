package com.example.service;

import com.example.model.Task;

import java.util.List;

public interface TaskService {

    List<Task> getAll();

    Task getById(long id);

    Task create(Task task);

    Task update(Task task);

    void deleteById(long id);
}
