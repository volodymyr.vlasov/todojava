package com.example.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "tasks")
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    @NotBlank
    @Size(min = 2, max = 10, message = "Size must be between 2 and 10")
    private String name;

    @Column(name = "description")
    @NotEmpty
    @Size(max = 15, message = "Max length - 15")
    private String description;

    @Column(name = "is_done")
    private boolean isDone;

    @Column(name = "author")
    @NotBlank
    @Size(min = 2, message = "Should be at least 2 symbols")
    private String author;

    @Column(name = "created_date")
    private LocalDateTime createdDate;
}
