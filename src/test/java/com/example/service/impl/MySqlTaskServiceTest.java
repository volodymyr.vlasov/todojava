package com.example.service.impl;

import com.example.model.Task;
import com.example.repository.TaskRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;

public class MySqlTaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private MySqlTaskService taskService;

    private Task task;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);

        task = new Task();

        task.setId(0);
        task.setName("Eat");
        task.setDescription("Eat food");
        task.setAuthor("Mykola");
    }

    @Test
    public void createTestShouldReturnTask() {

        task.setId(0);

        Mockito.when(taskRepository.save(task)).thenReturn(task);

        Task resultTask = taskService.create(task);

        Assertions.assertEquals(task, resultTask);
        Mockito.verify(taskRepository, Mockito.times(1)).save(task);
    }

    @Test
    public void createTestShouldReturnNull() {
        task.setId(1);

        Mockito.when(taskRepository.save(task)).thenReturn(task);

        Task resultTask = taskService.create(task);

        Mockito.verify(taskRepository, Mockito.times(0)).save(task);
        Assertions.assertNull(resultTask);
    }



}
